Template.map.helpers
  mapOptions: ->
    if GoogleMaps.loaded()
      return {
      center: new (google.maps.LatLng)(-37.8136, 144.9631)
      zoom: 8
      }
    return

Template.map.onCreated(->
  GoogleMaps.ready 'map', (map) ->
    google.maps.event.addListener map.instance, 'click', (event) ->
      Markers.insert
        lat: event.latLng.lat()
        lng: event.latLng.lng()
      return

    markers = {}

    Markers.find().observe
      added: (document) ->
        # Create a marker for this document
        marker = new google.maps.Marker(
          draggable: true
          animation: google.maps.Animation.DROP
          position: new (google.maps.LatLng)(document.lat, document.lng)
          map: map.instance
          id: document._id)
        # This listener lets us drag markers on the map and update their corresponding document.
        google.maps.event.addListener marker, 'dragend', (event) ->
          Markers.update marker.id, $set:
            lat: event.latLng.lat()
            lng: event.latLng.lng()
          return

        # Store this marker instance within the markers object.
        markers[document._id] = marker

      changed: (newDocument, oldDocument) ->
        markers[newDocument._id].setPosition
          lat: newDocument.lat
          lng: newDocument.lng
        return

      removed: (oldDocument) ->
        # Remove the marker from the map
        markers[oldDocument._id].setMap null
        # Clear the event listener
        google.maps.event.clearInstanceListeners markers[oldDocument._id]
        # Remove the reference to this marker instance
        delete markers[oldDocument._id]
        return
  return
)