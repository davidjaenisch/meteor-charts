Template.vis.onRendered(->
  width = 1000
  height = 75
  x = undefined
  drawCircles = undefined
  svg = d3.select('#circles').append('svg').attr('width', width).attr('height', height * Circles.find().count())
  drawCircles = (update) ->
    i = 0;
    Circles.find().forEach((doc)->
      data = doc.data
      circles = svg.selectAll(".circle" + i).data(data)
      unless update
        circles = circles.enter().append('circle').attr('class', 'circle' + i).attr('cx', (d, i) ->
          x(i)
        ).attr('cy', (i * height + 10) + (height / 2))
      else
        circles = circles.transition().duration(1000)
      circles.attr "r", (d) -> d
      i++
    )


  Circles.find().observe
    added: ->
      x = d3.scale.ordinal().domain(d3.range(Circles.findOne().data.length)).rangePoints([0, width], 1)
      drawCircles false
    changed: ->
      _.partial drawCircles true
)