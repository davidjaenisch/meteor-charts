Router.configure
  layoutTemplate: 'layout'
  loadingTemplate: 'loading'
  notFoundTemplate: 'notFound'
  yieldTemplates:
    nav:
      to: 'nav'
    footer:
      to: 'footer'
  waitOn: ->
    Meteor.subscribe 'circles'
    Meteor.subscribe 'markers'

Router.map ->
  @route 'map', path: '/map/'
  @route 'vis', path: '/vis/'
  @route 'entry', path: '/'
  @route 'private'

Router.onBeforeAction (->
  GoogleMaps.load()
  @next()
  return
), only: [
  'map'
]


Router.plugin 'ensureSignedIn', only: ['private']